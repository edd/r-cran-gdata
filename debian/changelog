gdata (3.0.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 22 Oct 2024 07:30:34 -0500

gdata (3.0.0-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 18 Oct 2023 08:36:56 -0500

gdata (2.19.0-2) unstable; urgency=medium

  * Rebuilding for unstable following bookworm release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 20 Jun 2023 06:14:41 -0500

gdata (2.19.0-1) experimental; urgency=medium

  * New upstream release (into 'experimental' while Debian is frozen)

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 10 May 2023 09:27:20 -0500

gdata (2.18.0.1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 14 May 2022 13:15:26 -0500

gdata (2.18.0-3) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 16 May 2020 06:44:38 -0500

gdata (2.18.0-2) unstable; urgency=medium

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Depends: on ${misc:Depends}
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/compat: Increase level to 9
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 03 Jun 2018 07:53:21 -0500

gdata (2.18.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for r-api-3.4 transition

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 29 Sep 2017 21:35:40 +0200

gdata (2.18.0-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Depends: on ${misc:Depends}

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 06 Jun 2017 12:53:32 -0500

gdata (2.17.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 08 Jul 2015 06:35:31 -0500

gdata (2.16.1-2) unstable; urgency=low

  * Rebuilding as pbuilder get persistent size mismatch

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 23 May 2015 08:59:28 -0500

gdata (2.16.1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 01 May 2015 21:01:17 -0500

gdata (2.13.3-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Apr 2014 13:36:03 -0500

gdata (2.13.2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 13 Jul 2013 12:20:09 -0500

gdata (2.12.0.2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Apr 2013 19:20:02 -0500

gdata (2.12.0-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 31 Mar 2013 07:30:48 -0500

gdata (2.12.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 12 Sep 2012 16:04:16 -0500

gdata (2.11.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 20 Jun 2012 06:21:43 -0500

gdata (2.10.6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 13 Jun 2012 09:42:30 -0500

gdata (2.10.5-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 10 Jun 2012 10:54:23 -0500

gdata (2.10.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Jun 2012 07:18:16 -0500

gdata (2.8.2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  
  * debian/control: Package changed to Architecture: all
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 24 Apr 2011 21:29:34 -0500

gdata (2.8.1-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 13 Nov 2010 14:36:59 -0600

gdata (2.8.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 05 May 2010 15:50:09 -0500

gdata (2.7.2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 02 May 2010 21:13:43 -0500

gdata (2.7.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 29 Jan 2010 08:57:12 -0600

gdata (2.7.0-1) unstable; urgency=low

  * New upstream release
  
  * debian/rules: No longer call /usr/share/cdbs/1/rules/dpatch.mk
  * debian/control: No longer use Build-Depends: on dpatch
  * debian/control: Package changed to Architecture: any

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 27 Jan 2010 20:07:26 -0600

gdata (2.6.1-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Nov 2009 15:59:19 -0600

gdata (2.6.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 20 Aug 2009 23:56:47 -0500

gdata (2.6.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 05 Aug 2009 09:18:04 -0500

gdata (2.4.2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 13 May 2008 19:09:35 -0500

gdata (2.4.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 30 Mar 2008 13:23:12 -0500

gdata (2.4.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.7.3

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 31 Jan 2008 06:42:11 -0600

gdata (2.3.1-2) unstable; urgency=low

  * debian/control: Add missing 'Depends: r-cran-gtools'   (Closes: #428336)

  * debian/control: Updated (Builds-)Depends updates to r-base-core (>= 2.5.0)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 11 Jun 2007 21:54:45 -0500

gdata (2.3.1-1) unstable; urgency=low

  * New upstream release
  * debian/control: (Builds-)Depends updated to r-base-core (>= 2.4.0)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 12 Nov 2006 20:39:09 -0600

gdata (2.3.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 20 Sep 2006 22:25:06 -0500

gdata (2.2.0-1) unstable; urgency=low

  * New upstream release

  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Hence Build-Depends: updated to r-base-dev (>= 2.3.1)

  * debian/post{inst,rm}: No longer call R to update html index
  * debian/watch: Updated to version 3
  
  * debian/control: Standards-Version: increased to 3.7.2

  * inst/{bin/xls2csv.bat,perl/xls2csv.pl}: s|/bin/env|/usr/bin/env|
  
 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 11 Aug 2006 19:56:55 -0500

gdata (2.1.2-1) unstable; urgency=low

  * New upstream release
  
  * debian/watch: Updated regular expression

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 14 Dec 2005 21:02:27 -0600

gdata (2.1.1-1) unstable; urgency=low

  * New upstream release
  * debian/control: Fixed typo in Description:
  * debian/post{inst,rm}: No longer call R to update html index

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 28 Oct 2005 22:20:27 -0500

gdata (2.0.8-1) unstable; urgency=low

  * Initial Debian release  [ but this package used to be part of the 
    gregmisc bundle packaged for Debian as r-cran-gregmisc ]

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  6 Sep 2005 21:16:48 -0500


